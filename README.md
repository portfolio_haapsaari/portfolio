# Portfolio

---

## Done

---

## Todo

### python

#### Machine learning library for python

- Add basic dense layer
- Add feed forward algorithm
- Add back-propagation algorithm

#### Machine learning snake

- Build snake using docker-compose, pipenv, python and pygame
- Collect states and inputs while playing the game
- Use neuroevolution

#### Recipe backend

- Build recipe backend using docker-compose, poetry, python, flask and sqlalchemy

---

### Angular

#### Recipe frontend

- Build recipe frontend using docker-compose and angular
